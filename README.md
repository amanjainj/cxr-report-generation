**This repository is a work in progress for chest x ray report generation.**

---
## Install the requirements
Run the following command -  
```
conda create --name <env> --file requirements.txt
```

## Finetune a language model

As a starting point, a language model (GPT-2) has been finetuned for the corpus of chest x-ray reports. Follow these steps to finetune a language model

1. Extract the reports  
        ```
        python data/prepare_data.py --output_dir data/raw/
        ```
2. Fine tune a language model  
        ```
        python language_modeling.py --raw_data_path data/raw --cache_dir data/cached/ --model gpt2 --output_dir checkpoints/gpt2-cxr
        ```
3. Use the fine-tuned model for generation  
        ```
        python run_generation.py --prompt "" --tokenizer gpt2 --model_path checkpoints/gpt2-cxr
        ```

## Finetune a language model with additional conditional features

Next, we construct a model that takes in additional features for language modeling. First, we use only the opacity lines from the reports. Follow these steps to train the model

#### Prepare data -  
1. Extract the opacity lines from reports and save them in json format. Name the files train_reports_opacity.json and test_reports_opacity.json and keep them in `raw_data_path`. Format - {'img_id': {'opacity':['line1','line2','line3'...]}}  
2. Generate the heatmaps and store in pickle files. The left and right lung heatmaps (gzip compressed) are assumed to be present in data['opacity']['orig_map'][0] and data['opacity']['orig_map'][1] respectively in the format - {'compressed_mask':bytes, 'mask_shape':tuple, 'dtype': numpy.dtype}  

#### Train the conditional model  
Run the following command -  
```
python language_modeling_conditional.py  
        --raw_data_path data/raw  
        --cache_dir data/cached/  
        --train_heatmaps_path /fast_data_111/cxr/3st_opacity_sides/*/artefacts/opacity/ensemble/*.pt  
        --test_heatmaps_path /fast_data5/processed/ChestXRays/test_set_results_v3/*/artefacts/opacity/ensemble/*.pt  
        --output_dir checkpoints/gpt2-cxr
```

- **raw_data_path** - Path to the directory containing train_reports_opacity.json and test_reports_opacity.json  
- **train_heatmaps_path** - regex path to the pickle files containing the heatmaps for train set (in the format described in step 1.2,1.3 above)  
- **test_heatmaps_path** - regex path to the pickle files containing the heatmaps for test set (in the format described in step 1.2,1.3 above)  
- **cache_dir** - Path to the directory containing cached datasets (or where to store the cache if not present)  
- **output_dir** - Path to store the model checkpoints  

#### Use the trained model for generation  
Run the following command -  
```
python run_generation_conditional.py  
        --prompt ""  
        --heatmap_file_path ../misc/test.pt  
        --tokenizer gpt2  
        --model_path checkpoints/gpt2-cxr-conditional/pytorch_model.bin  
        --max_length 1000  
```  

- **prompt** - Prompt to be used for generation  
- **heatmap_file_path** - Path to the file containing the heatmaps to be used for generation (in the format described in step 1.2,1.3 above)  
- **tokenizer** - The tokenizer that will be used to encode data for the model  
- **model_path** - The path to model's state dict file to load the model that will be used to make predictions  
- **max_length** - The max length of generated output  

#### Evaluate the model on test reports  
Run the following command -  
```
python evaluate_generation.py  
        --prompt ""  
        --test_heatmaps_path /fast_data5/processed/ChestXRays/test_set_results_v3/*/artefacts/opacity/ensemble/*.pt  
        --test_data_path data/raw/test_reports_opacity.json
        --tokenizer gpt2  
        --model_path checkpoints/gpt2-cxr-conditional/pytorch_model.bin    
        --max_length 1000  
        --results_file_path results/evaluate_generation_results.txt  
```

- **prompt** - Prompt to be used for generation  
- **test_heatmaps_path** - regex path to the pickle files containing the heatmaps for test set (in the format described in step 1.2,1.3 above)  
- **test_data_path** - Path to the test_reports_opacity.json file  
- **tokenizer** - The tokenizer that will be used to encode data for the model  
- **model_path** - The path to model's state dict file to load the model that will be used to make predictions  
- **max_length** - The max length of generated output   
- **results_file_path** - Path to the file to store the results  

## Train a custom tokenizer
To train a custom tokenizer for our dataset, follow these steps

1. Concat all the text from the training set and write in a file.
2. Run the tokenizer training script  
        ```
        python tokenizer/train_tokenizer.py --raw_data_path data/raw/ --output_dir tokenizer/gpt2-cxr-tokenizer
        ```
