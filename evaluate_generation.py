import torch

from transformers import AutoTokenizer, AutoModel
from transformers.configuration_gpt2 import GPT2Config

from models.GPT2LMHeadConditional import GPT2LMHeadConditionalModel

from pipelines.ConditionalTextGenerationPipeline import ConditionalTextGenerationPipeline

import argparse
import json
from glob import glob
from tqdm import tqdm

from transformers import logging
logging.set_verbosity_error()

# Get args
parser = argparse.ArgumentParser()

parser.add_argument("--prompt", default="", type=str, required=False, help="Prompt for text generation")

parser.add_argument("--test_heatmaps_path", default='/fast_data5/processed/ChestXRays/test_set_results_v3/*/artefacts/opacity/ensemble/*.pt', type=str, required=False, help="Path to the artefacts for test set")
parser.add_argument("--test_data_path", default='data/raw/test_reports_opacity.json', type=str, required=False, help="Path to the raw test set reports")

parser.add_argument("--tokenizer", default='gpt2', type=str, required=False, help="Tokenizer to use")

parser.add_argument("--model_path", default='checkpoints/gpt2-cxr-conditional/pytorch_model.bin', type=str, required=False, help="Path to the model")

parser.add_argument("--max_length", default=1000, type=int, required=False, help="Max length of generated text")

parser.add_argument("--results_file_path", default='results/evaluate_generation_results.txt', type=str, required=False, help="File path to store the results")

args = parser.parse_args()


# Set configuration
config = GPT2Config()

# Initialize model
model = GPT2LMHeadConditionalModel(config)

#Load model weights
print("Loading model weights")
model.load_state_dict(torch.load(args.model_path))
model.eval()

# Initialize the tokenizer
tokenizer = AutoTokenizer.from_pretrained(args.tokenizer, pad_token='<|endoftext|>')

# Initialize the pipeline
generator = ConditionalTextGenerationPipeline(
	model=model,
	tokenizer=tokenizer
)

results = ""

reports_data = json.load(open(args.test_data_path))

heatmaps_files_regex = args.test_heatmaps_path 

# Filter out the files for which there is no report or no opacity line in the report
heatmaps_files = [ f for f in glob(heatmaps_files_regex) if f.split("/")[-1][:-3] in reports_data and reports_data[f.split("/")[-1][:-3]]["opacity"]] 

# Evaluation is done for only first 50 examples for now. 
heatmaps_files = heatmaps_files[:50]

print("Evaluating Model on test set")
for heatmaps_file in tqdm(heatmaps_files):

	img_id = heatmaps_file.split("/")[-1][:-3]

	gold_report = ". ".join(list(set([l.split("lungs:")[-1].strip() for l in reports_data[img_id]["opacity"]])))

	# prompt = " ".join(gold_report.split()[:3])  # give first 3 words of gold report as prompt

	generated_text = generator(args.prompt,heatmaps_file,config={'max_length':args.max_length})[0]["generated_text"]

	results += "ID : {}\nGold Report : {}\nGenerated Report : {}\n\n\n".format(img_id,gold_report,generated_text)

print("Writing results at {}".format(args.results_file_path))
with open(args.results_file_path,'w') as f:
	f.write(results)

