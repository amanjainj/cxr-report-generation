from transformers import AutoTokenizer, AutoModelWithLMHead
from transformers import Trainer, TrainingArguments
from transformers import TextDataset, DataCollatorForLanguageModeling

from transformers.configuration_gpt2 import GPT2Config

import argparse
from os.path import join

from ConditionalTextDataset import ConditionalTextDataset
from models.GPT2LMHeadConditional import GPT2LMHeadConditionalModel


# Get args
parser = argparse.ArgumentParser()

parser.add_argument("--raw_data_path", default='data/raw/', type=str, required=False, help="Path to the raw train and test set reports")
parser.add_argument("--train_heatmaps_path", default='/fast_data_111/cxr/3st_opacity_sides/*/artefacts/opacity/ensemble/*.pt', type=str, required=False, help="Path to the artefacts for train set")
parser.add_argument("--test_heatmaps_path", default='/fast_data5/processed/ChestXRays/test_set_results_v3/*/artefacts/opacity/ensemble/*.pt', type=str, required=False, help="Path to the artefacts for test set")

parser.add_argument("--cache_dir", default='data/cached/', type=str, required=False, help="Path to the directory to save the cached dataset")

parser.add_argument("--output_dir", default='checkpoints/gpt2-cxr-conditional', type=str, required=False, help="Path to the directory to save checkpoints")

args = parser.parse_args()



tokenizer = AutoTokenizer.from_pretrained('gpt2', pad_token='<|endoftext|>')
max_length = 128  # max tokens for each text report (opacity line)

# path to the reports
train_reports_path = join(args.raw_data_path,"train_reports_opacity.json")
test_reports_path = join(args.raw_data_path,"test_reports_opacity.json")

# path to the cached dataset
cache_path_train = join(args.cache_dir,"GPT2Conditional_train_{}.pt".format(max_length))
cache_path_test = join(args.cache_dir,"GPT2Conditional_test_{}.pt".format(max_length))


# Load dataset
train_dataset = ConditionalTextDataset(tokenizer, max_length, cache_path_train, train_reports_path, args.train_heatmaps_path)
test_dataset = ConditionalTextDataset(tokenizer, max_length, cache_path_test, test_reports_path, args.test_heatmaps_path)

print("Dataset Loaded. Size - {} (train), {} (test)".format(len(train_dataset),len(test_dataset)))


# Set configuration
config = GPT2Config()


# Initialize model
model = GPT2LMHeadConditionalModel(config)

# Freeze the parameters of transformer, to just train the fc and lm_head layers.
#for param in model.transformer.parameters():
#    param.requires_grad = False


# Prepare training argumemts
training_args = TrainingArguments(
    output_dir=args.output_dir, #The output directory where model is saved
    overwrite_output_dir=True, 
    num_train_epochs=3, 
    per_device_train_batch_size=16, 
    per_device_eval_batch_size=32,  
    eval_steps = 400, # Number of update steps between two evaluations.
    save_steps=800, # after # steps model is saved
    learning_rate=5e-4,
    warmup_steps=500, # warmup steps for lr scheduler
    dataloader_drop_last=True,
    )


# Initialize the Trainer
trainer = Trainer(
    model=model,
    args=training_args,
    # data_collator=data_collator,
    train_dataset=train_dataset,
    eval_dataset=test_dataset,
    prediction_loss_only=True,
)

# Training
trainer.train()

# Save the model
trainer.save_model()




