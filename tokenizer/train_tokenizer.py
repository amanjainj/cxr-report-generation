import json
import os
from os.path import join
import argparse

from tokenizers import ByteLevelBPETokenizer


def create_tokenizer_training_file(raw_data_path,train_reports_path_tokenizer):
	train_reports_opacity = json.load(open(join(raw_data_path,"train_reports_opacity.json")))
	train_reports_opacity = "\n".join([". ".join(v["opacity"]) for k,v in train_reports_opacity.items() if v["opacity"]])
	train_reports_opacity = train_reports_opacity.lower()
	with open(train_reports_path_tokenizer,"w") as f:
		f.write(train_reports_opacity)

# Get args
parser = argparse.ArgumentParser()

parser.add_argument("--raw_data_path", default='../data/raw/', type=str, required=False, help="Path to the raw train and test set reports")

parser.add_argument("--output_dir", default='gpt2-cxr-tokenizer', type=str, required=False, help="Path to the directory to save checkpoints")

args = parser.parse_args()


# path to the training reports (all text concatenated to train the tokenizer)
train_reports_path_tokenizer = join(args.raw_data_path,"train_reports_opacity_tokenizer.txt")

# if the above file is not present, create it
if not os.path.exists(train_reports_path_tokenizer):
	create_tokenizer_training_file(args.raw_data_path,train_reports_path_tokenizer)


# Initialize a tokenizer
tokenizer = ByteLevelBPETokenizer()

# Customize training
tokenizer.train(files=[train_reports_path_tokenizer], vocab_size=5000, min_frequency=2, special_tokens=["<|endoftext|>"])

# Save files to disk
tokenizer.save_model(args.output_dir)

