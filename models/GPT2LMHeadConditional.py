import warnings

import torch
import torch.nn as nn
from torch.nn import CrossEntropyLoss

from transformers.modeling_outputs import CausalLMOutputWithPast
from transformers.utils import logging
from transformers import GPT2PreTrainedModel, GPT2Model

from utils.generation_utils import ConditionalGenerationMixin

class GPT2LMHeadConditionalModel(GPT2PreTrainedModel,ConditionalGenerationMixin):
    """
    Architecture for conditional text generation
    Skeleton taken from GPT2LMHeadModel. 
    Made required changes to incorporate the additional conditional features
    """

    def __init__(self, config):
        super().__init__(config)
        self.transformer = GPT2Model.from_pretrained("gpt2")

        # The shape of input heatmap tensor
        self.conditional_feature_shape = [1600,3200]

        # The CNN layers heatmap is passed through
        self.cnn_layers = nn.Sequential(
            # First convolution layer
            nn.Conv2d(1, 4, kernel_size=10, stride=2, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=10, stride=10),

            # Second convolution layer
            nn.Conv2d(4, 8, kernel_size=5, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=10, stride=10),
        )

        # Linear layer taking concat(heatmap features passed through CNN layers, transformer's final state) as input
        self.fc = nn.Linear(8*7*15 + config.n_embd, config.n_embd, bias=False)

        # The final linear layer projecting to the vocab size for softmax
        self.lm_head = nn.Linear(config.n_embd, config.vocab_size, bias=False)

        # Activation function used 
        self.tanh = nn.Tanh()

        self.init_weights()


    def get_output_embeddings(self):
        return self.lm_head


    def forward(
        self,
        input_ids=None,
        past_key_values=None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        head_mask=None,
        inputs_embeds=None,
        encoder_hidden_states=None,
        encoder_attention_mask=None,
        labels=None,
        use_cache=None,
        output_attentions=None,
        output_hidden_states=None,
        return_dict=None,
        conditional_features=None,
        **kwargs,
    ):
        r"""
        labels (:obj:`torch.LongTensor` of shape :obj:`(batch_size, sequence_length)`, `optional`):
            Labels for language modeling.
            Note that the labels **are shifted** inside the model, i.e. you can set ``labels = input_ids``
            Indices are selected in ``[-100, 0, ..., config.vocab_size]``
            All labels set to ``-100`` are ignored (masked), the loss is only
            computed for labels in ``[0, ..., config.vocab_size]``
        """
        if "past" in kwargs:
            warnings.warn(
                "The `past` argument is deprecated and will be removed in a future version, use `past_key_values` instead.",
                FutureWarning,
            )
            past_key_values = kwargs.pop("past")
        assert kwargs == {}, f"Unexpected keyword arguments: {list(kwargs.keys())}."
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        # print(input_ids.shape)

        transformer_outputs = self.transformer(
            input_ids,
            past_key_values=past_key_values,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            encoder_hidden_states=encoder_hidden_states,
            encoder_attention_mask=encoder_attention_mask,
            use_cache=use_cache,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
            return_dict=return_dict,
        )
        hidden_states = transformer_outputs[0]

        # pass conditional features through cnn layers
        conditional_features = self.cnn_layers(conditional_features.unsqueeze(1))
        # print(conditional_features.shape)

        # Flatten to get B X 768 tensor
        conditional_features = conditional_features.view(conditional_features.size(0), -1)

        # Repeat to be able to cat with hidden_states
        conditional_features = conditional_features.unsqueeze(1).repeat(1,hidden_states.shape[1],1)

        # Concat the hidden states and conditional_features
        hidden_states = torch.cat((hidden_states,conditional_features), dim=-1)

        lm_logits = self.lm_head(self.tanh(self.fc(hidden_states)))

        loss = None
        if labels is not None:
            # Shift so that tokens < n predict n
            shift_logits = lm_logits[..., :-1, :].contiguous()
            shift_labels = labels[..., 1:].contiguous()
            # Flatten the tokens
            loss_fct = CrossEntropyLoss()
            loss = loss_fct(shift_logits.view(-1, shift_logits.size(-1)), shift_labels.view(-1))

        if not return_dict:
            output = (lm_logits,) + transformer_outputs[1:]
            return ((loss,) + output) if loss is not None else output

        return CausalLMOutputWithPast(
            loss=loss,
            logits=lm_logits,
            past_key_values=transformer_outputs.past_key_values,
            hidden_states=transformer_outputs.hidden_states,
            attentions=transformer_outputs.attentions,
        )

