import torch

from transformers import AutoTokenizer, AutoModel
from transformers.configuration_gpt2 import GPT2Config

from models.GPT2LMHeadConditional import GPT2LMHeadConditionalModel

from pipelines.ConditionalTextGenerationPipeline import ConditionalTextGenerationPipeline

import argparse

from transformers import logging
logging.set_verbosity_error()

# Get args
parser = argparse.ArgumentParser()

parser.add_argument("--prompt", default="", type=str, required=False, help="Prompt for text generation")

parser.add_argument("--heatmap_file_path", default="/home/users/aman.jain/misc/test.pt", type=str, required=False, help="Path to the heatmap file to be used for generation")

parser.add_argument("--tokenizer", default='gpt2', type=str, required=False, help="Tokenizer to use")

parser.add_argument("--model_path", default='checkpoints/gpt2-cxr-conditional/pytorch_model.bin', type=str, required=False, help="Path to the model")

parser.add_argument("--max_length", default=1000, type=int, required=False, help="Max length of generated text")
args = parser.parse_args()


# Set configuration
config = GPT2Config()

# Initialize model
model = GPT2LMHeadConditionalModel(config)

#Load model weights
print("Loading model weights")
model.load_state_dict(torch.load(args.model_path))
model.eval()

# Initialize the tokenizer
tokenizer = AutoTokenizer.from_pretrained(args.tokenizer, pad_token='<|endoftext|>')

# Initialize the pipeline
generator = ConditionalTextGenerationPipeline(
	model=model,
	tokenizer=tokenizer
)

# Run generation
generated_text = generator(args.prompt,args.heatmap_file_path,config={'max_length':args.max_length})[0]["generated_text"]

print("Generated Text : {}".format(generated_text))

