from os import listdir
from os.path import join
from tqdm import tqdm
import json
import re
import argparse


# Get args
parser = argparse.ArgumentParser()

parser.add_argument("--output_dir", default='data/raw/', type=str, required=False, help="Directory to write the train set reports")

args = parser.parse_args()


all_reports_dir_train = "/fast_data5_2/processed/ChestXRays/training_reports_all/"
all_reports_dir_test = "/fast_data5_2/processed/ChestXRays/testing_reports/"


# Only use reports from max
all_max_reports_train = [f for f in listdir(all_reports_dir_train) if "max" in f]
all_max_reports_test = [f for f in listdir(all_reports_dir_test) if "max" in f]


# extract the text from all the reports. Filter out unwanted lines.
def prepare_data(all_reports,all_reports_dir):
	data = "" #String to store the contents of report

	for file in tqdm(all_reports):
		full_path = join(all_reports_dir, file)

		f = open(full_path, 'r') 
		Lines = f.readlines() 

		first = True

		for line in Lines: 
			l = line.strip()
			if "|X-Ray Chest" in l and first:
				first = False
				continue

			elif "|X-Ray Chest" in l and not first:
				# The report is repeated twice in the file, no need to read the same repeated text again
				break 

			l = l.strip()
			l = re.sub(r"\s", " ", l)

			if l:				
				data += l + " "

		data += "\n" #Each report is separated by a newline

	return data


train_data = prepare_data(all_max_reports_train,all_reports_dir_train)
test_data = prepare_data(all_max_reports_test,all_reports_dir_test)


# Write the extracted filtered text from all the reports in a file
train_path = join(args.output_dir,"train_data.txt")
test_path = join(args.output_dir,"test_data.txt")

with open(train_path,'w') as f:
	f.write(train_data)

with open(test_path,'w') as f:
	f.write(test_data)

