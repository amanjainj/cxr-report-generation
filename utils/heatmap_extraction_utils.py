import torch

import numpy as np
import gzip


def np_decompress(comp_dict):
    """
    decompresses a compressed numpy array
    Args:
        comp_dict: output of np_compress, a dictionary of form
        {'compressed_mask':bytes, 'mask_shape':tuple, 'dtype': numpy.dtype}

    Returns:
        uncompressed mask of shape comp_dict['mask_shape']

    """

    byte_mask = gzip.decompress(comp_dict["compressed_mask"])
    flattened_mask = np.frombuffer(byte_mask, dtype=comp_dict["dtype"])
    orig_mask = flattened_mask.reshape(comp_dict["mask_shape"])
    return orig_mask


def get_heatmap(data_file):
    """
    gets heatmap tensor from a data file
    Args:
        data_file: path to the file to extract heatmap from,
        Containing the left and right lung heatmaps (gzip compressed) 
        in data['opacity']['orig_map'][0] and data['opacity']['orig_map'][1] respectively

    Returns:
        A torch tensor containing the heatmap

    """

    data = torch.load(data_file)
    left_compressed_pixmap = data['opacity']['orig_map'][0]
    right_compressed_pixmap = data['opacity']['orig_map'][1]
    left_uncompressed_pixmap = np_decompress(left_compressed_pixmap)
    right_uncompressed_pixmap = np_decompress(right_compressed_pixmap)

    left_uncompressed_pixmap = torch.tensor(np.array(left_uncompressed_pixmap), dtype=torch.float32)
    right_uncompressed_pixmap = torch.tensor(np.array(right_uncompressed_pixmap), dtype=torch.float32)

    heatmap = torch.cat((left_uncompressed_pixmap,right_uncompressed_pixmap),dim=1)

    return heatmap
