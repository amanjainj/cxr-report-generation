from transformers import pipeline
import argparse

# Get args
parser = argparse.ArgumentParser()

parser.add_argument("--prompt", default="Results: ", type=str, required=False, help="Prompt for text generation")

parser.add_argument("--tokenizer", default='gpt2', type=str, required=False, help="Tokenizer to use")

parser.add_argument("--model_path", default='checkpoints/gpt2-cxr', type=str, required=False, help="Path to the model")

args = parser.parse_args()

# Initialize the pipeline
generator = pipeline(
	'text-generation',
	model=args.model_path,
	tokenizer=args.tokenizer,
	config={'max_length':800}
)


print(generator(args.prompt))
