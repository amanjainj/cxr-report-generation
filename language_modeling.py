from transformers import AutoTokenizer, AutoModelWithLMHead
from transformers import Trainer, TrainingArguments
from transformers import TextDataset, DataCollatorForLanguageModeling

import argparse
from os.path import join


def load_dataset(train_path,test_path,tokenizer, cache_dir):
    train_dataset = TextDataset(
          tokenizer=tokenizer,
          file_path=train_path,
          block_size=128,
          overwrite_cache=False,
          cache_dir=cache_dir)
     
    test_dataset = TextDataset(
          tokenizer=tokenizer,
          file_path=test_path,
          block_size=128,
          overwrite_cache=False,
          cache_dir=cache_dir)   
    
    data_collator = DataCollatorForLanguageModeling(
        tokenizer=tokenizer, mlm=False,
    )
    return train_dataset,test_dataset,data_collator


# Get args
parser = argparse.ArgumentParser()

parser.add_argument("--raw_data_path", default='data/raw/', type=str, required=False, help="Path to the raw train and test set reports")

parser.add_argument("--cache_dir", default='data/cached/', type=str, required=False, help="Path to the directory to save the cached dataset")

parser.add_argument("--model", default='gpt2', type=str, required=False, help="Pretrained model to use")

parser.add_argument("--output_dir", default='checkpoints/gpt2-cxr', type=str, required=False, help="Path to the directory to save checkpoints")

args = parser.parse_args()



tokenizer = AutoTokenizer.from_pretrained(args.model)

train_path = join(args.raw_data_path,"train_data.txt")
test_path = join(args.raw_data_path,"test_data.txt")
cache_dir = args.cache_dir

# Load dataset
train_dataset, test_dataset, data_collator = load_dataset(train_path, test_path, tokenizer, cache_dir)


# Initialize model
model = AutoModelWithLMHead.from_pretrained(args.model)


# Prepare training argumemts
training_args = TrainingArguments(
    output_dir=args.output_dir, #The output directory where model is saved
    overwrite_output_dir=True, 
    num_train_epochs=3, 
    per_device_train_batch_size=16, 
    per_device_eval_batch_size=32,  
    eval_steps = 400, # Number of update steps between two evaluations.
    save_steps=800, # after # steps model is saved 
    warmup_steps=500, # warmup steps for lr scheduler
    dataloader_drop_last=True,
    )


# Initialize the Trainer
trainer = Trainer(
    model=model,
    args=training_args,
    data_collator=data_collator,
    train_dataset=train_dataset,
    eval_dataset=test_dataset,
    prediction_loss_only=True,
)

# Training
trainer.train()

# Save the model
trainer.save_model()


