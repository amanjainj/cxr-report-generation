import torch
from torch.utils.data import Dataset

import os
import json
from tqdm import tqdm
import pickle
from glob import glob

from utils.heatmap_extraction_utils import get_heatmap

class ConditionalTextDataset(Dataset):
	"""
	Dataset class for our architecture
	"""

	def __init__(self, tokenizer, max_length, path, reports_file, heatmaps_path):

		self.tokenizer = tokenizer

		# Max length of the opacity line in report. Excess tokens will be truncated
		self.max_length = max_length

		self.heatmaps_path = heatmaps_path

		# Load the cached dataset if present, else preprocess 
		if os.path.exists(path):
			print("Loading from the cached data found")
			self.data = pickle.load(open(path,'rb'))
		else:
			print("No cached data found. Preprocessing data now")
			self.data = self.preprocess(reports_file, heatmaps_path)
			with open(path,'wb') as f:
				pickle.dump(self.data,f)


	def preprocess(self, reports_file, heatmaps_path):
		"""
		Preprocess the data

		Args:
        	reports_file: path to the json file (containing opacity lines), 
        	a dictionary of form - {'img_id': {'opacity':['line1','line2','line3'...]}}
        	heatmaps_path: regex path to the files containing heatmaps,
        	files are named as {img_id}.pt
​​
    	Returns:
    		a dictionary of form -
    		{
				"img_id" : list of image ids,
				"input_ids" : torch tensor of encoded opacity lines,
				"attention_mask" : torch tensor of attention mask 
			}
		"""

		list_img_id = []
		list_input_ids = []
		list_attention_mask = []
		list_conditional_features = []

		reports_data = json.load(open(reports_file))

		# Extract the img_ids for which heatmap is present
		heatmaps_files_ids = [f.split("/")[-1][:-3] for f in glob(heatmaps_path)]

		for img_id,lines in tqdm(reports_data.items()):

			if not img_id in heatmaps_files_ids:
				# If heatmap is not present, do not include in the dataset
				continue

			opacity_lines = lines["opacity"]
			
			if not opacity_lines:
				# continue if no opacity lines present
				continue 

			text = self.get_single_string(opacity_lines)
			text = self.clean_text(text)

			encoding = self.tokenizer(text, add_special_tokens=True, truncation=True, max_length=self.max_length, padding='max_length')
			list_input_ids.append(encoding["input_ids"])
			list_attention_mask.append(encoding["attention_mask"])

			list_img_id.append(img_id)

			# list_conditional_features.append(torch.randn(768)) #using random features for now

		return {
			"img_id" : list_img_id,
			"input_ids" : torch.tensor(list_input_ids, dtype=torch.long), 
			"attention_mask" : torch.tensor(list_attention_mask, dtype=torch.long), 
			# "conditional_features" : torch.stack(list_conditional_features),
		}


	def get_single_string(self,opacity_lines):
		"""
		From all the multiple opacity lines, create a single string to be used for training

		Startegy 1 - choose the longest line
		Strategy 2 - concat all the lines (with some cleanup)

		Args:
			opacity_lines : list of opacity lines present in the report

		Returns:
			the string created from the opacity lines
		"""

		## Startegy 1 
		# opacity_lines = list(set([l.split("lungs:")[-1].strip() for l in opacity_lines]))
		# text = max(opacity_lines, key=len)

		## Startegy 2
		text = ".".join(list(set([l.split("lungs:")[-1].strip() for l in opacity_lines])))

		return text


	def clean_text(self,text):
		"""
		Do any cleaning of the text if required

		Args:
			text: the raw string 

		Returns:
			the cleaned up string
		"""

		return text


	def __len__(self):
		return self.data['input_ids'].shape[0]


	def __getitem__(self, id):

		input_ids = self.data['input_ids'][id]

		labels = input_ids.clone().detach()
		labels[labels == self.tokenizer.pad_token_id] = -100

		img_id = self.data['img_id'][id]

		heatmaps_path = "/".join(self.heatmaps_path.split("/")[:-1])

		# Create the string for the path to the heatmap using the img_id
		data_file = glob(heatmaps_path + "/" + str(img_id) + ".pt")[0]

		# Extract the heatmap tensor
		heatmap = get_heatmap(data_file)

		return {
			'input_ids': input_ids,
			'attention_mask': self.data['attention_mask'][id],	
			'conditional_features': heatmap,	
			'labels': labels,		
		}




